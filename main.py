#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
A simple ISP pipeline including ...
'''
import os
import argparse
import matplotlib
from matplotlib import pyplot as plt
from modules.DPC import *
from modules.BLC import *
from modules.AWB import *
from modules.Demosaic import *
from modules.CCM import *
from modules.Gamma import *
from modules.CSC import *
from modules.BGNR import *
from modules.util import *
from modules.LSC import *
from modules.Formatter import *
from modules.HSC import *
from modules.BCC import *
from modules.EE import *

from numpy.ctypeslib import ndpointer
import ctypes

import numpy as np
from tqdm import tqdm
import cv2

#global config
parser = argparse.ArgumentParser()
parser.add_argument('-spath', default="./rawdata/chart_toyworld_d65_1500lux_016.RAWMIPI", type=str, help="Input image raw data.")
parser.add_argument('-opath', default="./output/encode.jpg", type=str, help="Onput jpeg image.")
parser.add_argument('-raw_w', default=4000, type=int, help="Input raw data's width.")
parser.add_argument('-raw_h', default=3000, type=int, help="Input raw data's hight")
parser.add_argument('-bayer', default="rggb", type=str, help="Bayer pattern of raw data.")
parser.add_argument('-bw', default=1023, type=int, help="Bit width of raw data.")
#dead piexl correction (DPC) config
parser.add_argument('-dpc_thd', default=30, type=int, help="Dead pixel correction diff threshold")
#lsc config
parser.add_argument('-is_cdll', default=True, type=int, help="Lsc use c dll")
#black level compensation (BLC) config
parser.add_argument('-r_offset', default=-64, type=int, help="Black level offset of R channel")
parser.add_argument('-gr_offset', default=-64, type=int, help="Black level offset of Gr channel")
parser.add_argument('-gb_offset', default=-64, type=int, help="Black level offset of Gb channel")
parser.add_argument('-b_offset', default=-64, type=int, help="Black level offset of B channel")
parser.add_argument('-alpha', default=0, type=int, help="Fusion parameter for R channel")
parser.add_argument('-beta', default=0, type=int, help="Fusion parameter for B channel")
#auto white balance (AWB) gain control
parser.add_argument('-r_gain', default= 1.98467, type=int, help="AWB Gain for R channel")
parser.add_argument('-gr_gain', default=1.009277, type=int, help="AWB Gain for Gr channel")
parser.add_argument('-gb_gain', default=1.009277, type=int, help="AWB Gain for Gb channel")
parser.add_argument('-b_gain', default=1.695867, type=int, help="AWB Gain for B channel")
#demosaic
parser.add_argument('-interplate', default='malvar', type=str, help="Interplate mothod for bayer image (malvar or bilinear)")
#gamma corretion
parser.add_argument('-gamma', default=0.6, type=int, help="Gamma ratio")
#noise reduce for yuv(luminance) domain using bilateral gaussian filter
parser.add_argument('-diameter', default=9, type=int, help="bilateral gaussian filter diameter.")
parser.add_argument('-color_sigma', default=10, type=int, help="Bilateral gaussian filter color sigma.")
parser.add_argument('-space_sigma', default=10, type=int, help="Bilateral gaussian filter space sigma.")
# edge enhance using sobel filter
parser.add_argument('-extent', default=0.05, type=float, help="The enhance entent for edge")
#hue and saturation control
parser.add_argument('-hue', default=4, type=int, help="Hue gain.")
parser.add_argument('-saturation', default=3, type=int, help="Saturation gain.")
#britness and contast control
parser.add_argument('-bright', default=5, type=int, help="Britness gain.")
parser.add_argument('-contrast', default=5, type=int, help="Contast gain.")

#jpeg encoder quality
parser.add_argument('-quality', default=96, type=int, help="Jpeg encoder quality.")
args = parser.parse_args()

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # read 10bit from mipi raw data
    print('****** read 10 bit raw data ******')
    raw_img = read_uint10(args.spath)
    raw_img = raw_img.reshape(args.raw_h, args.raw_w)
    cv2.imwrite('./output/raw_img.png', raw_img)

    # # dead pixel correction
    # print('****** do dead pixel correction ******')
    # dpc = DPC(raw_img, args)
    # dpc_img = dpc.do()
    # cv2.imwrite('./output/dpc_img.png', dpc_img)

    # black level compensation
    print('****** do black level compensation ******')
    blc = BLC(raw_img, args)
    blc_img = blc.do()
    cv2.imwrite('./output/blc_img.png', blc_img)


    # lens shading correction
    print('****** do lens shading correction ******')
    if not args.is_cdll:
        if os.path.isfile('./modules/lsc_gain_interp.npz'):
            lsc_gain = np.load('./modules/lsc_gain_interp.npz')
            print(lsc_gain['r'][0, 0], lsc_gain['gr'][0, 0], lsc_gain['gb'][0, 0], lsc_gain['b'][0, 0])
        else:
            lsc_gain = np.load('./modules/lsc_gain.npz')
            get_block_interplate(args.w, args.h, lsc_gain)
            lsc_gain = np.load('./modules/lsc_gain_interp.npz')
        lsc = LSC(blc_img, args, lsc_gain)
        lsc_img = lsc.do()
    else:
        lscdll = ctypes.CDLL('./modules/clsc.dll')
        blc_img = np.asarray(blc_img.reshape(-1),dtype=np.uint16)
        cptr = blc_img.ctypes.data_as(ctypes.POINTER(ctypes.c_uint16))
        lscdll.ExcuteProcess(cptr, args.raw_w, args.raw_h)
        lsc_img = np.ctypeslib.as_array(cptr, shape=(args.raw_h * args.raw_w, ))
        lsc_img = lsc_img.reshape(args.raw_h, args.raw_w, order='C')
    cv2.imwrite('./output/ccclsc_img.png', lsc_img)

    # auto white balance gain control
    print('****** do auto white balance ******')
    awb = AWB(lsc_img, args)
    awb_img = awb.do()
    cv2.imwrite('./output/awb_img.png', awb_img)

    # demosaic
    print('****** do demosaic ******')
    demosaic = Demosaic(awb_img, args)
    dem_img = demosaic.do()
    save_rgb_img('./output/dem_img.png', dem_img)


    # color correction matrix
    print('****** do color correction ******')
    # color_matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    # ccm = CCM(dem_img,color_matrix)
    # ccm_img = ccm.do()
    # save_rgb_img('./output/ccm_img.png', ccm_img)

    # gamma correction
    print('****** do gamma correction ******')
    limit = args.bw+1
    val = [round(pow(float(i) / float(limit), args.gamma) * limit) for i in range(0, limit)]
    lut = dict(zip(range(0, limit), val))
    gc = Gamma(dem_img, args, lut)
    gc_img = gc.do()
    save_rgb_img('./output/gc_img_.png', gc_img)

    # Color Space Conversion (RGB domain -> YUV domain)
    print('****** RGB domain to YUV domain ******')
    csc = CSC(gc_img)
    ycbcr_img = csc.do()
    np.save('./output/yuv.npy', ycbcr_img)
    save_rgb_img('./output/yuv_img.png', ycbcr_img)

    # noise reduce for YUV luminance channel
    # print('****** noise reduce for YUV luminance channel ******')
    # bgnr = BGNR(ycbcr_img[:, :, 0], args)
    # nr_yuvimg = bgnr.do()
    # np.save('./output/yuv_img_nr.npy', nr_yuvimg)
    # save_rgb_img('./output/yuv_img_nr.png', nr_yuvimg)

    # edge enhance
    print('****** edge enhance for luminance channel ******')
    ee = EE(ycbcr_img[:, :, 0], args)
    ee_yuvimg = ee.do()
    np.save('./output/yuv_img_ee.npy', ee_yuvimg)
    # save_rgb_img('./output/yuv_img_ee.png', ee_yuvimg)


    # hue/saturation control
    print('****** hue/saturation control for chrome channel ******')
    ycbcr_img = np.load('./output/yuv.npy')
    hsc = HSC(ycbcr_img[:, :, 1:3], args)
    hsc_yuvimg = hsc.execute()
    np.save('./output/hsc_img.npy', hsc_yuvimg)
    # save_rgb_img('./output/hsc_img.png', hsc_yuvimg)

    # brighyness/contrast control
    print('****** brighyness/contrast control for luminance channel ******')
    ee_yuvimg = np.load('./output/yuv_img_ee.npy')
    bcc = BCC(ee_yuvimg, args)
    bcc_yuvimg = bcc.execute()
    np.save('./output/bcc_yuvimg.npy', bcc_yuvimg)
    #save_rgb_img('./output/yuvimg_bcc_img.png', bcc_yuvimg)


    save_yuv_img('./output/yuvimg_bcc_hsc_img.png', np.load('./output/bcc_yuvimg.npy'), np.load('./output/hsc_img.npy'))
    
    
    # jpeg encode compression
    print('****** jpeg encode compression ******')
    yuvimg_out = np.empty((args.raw_h, args.raw_w, 3), dtype=np.uint8)
    # bcc_yuvimg = np.load('./output/bcc_yuvimg.npy')
    # hsc_yuvimg = np.load('./output/hsc_img.npy')
    yuvimg_out[:,:,0] = bcc_yuvimg
    yuvimg_out[:,:,1:3] = hsc_yuvimg
    formatter = Formatter(yuvimg_out, args)
    encode_img = formatter.encode()
    




















