#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class Gamma:
    '''
    doing Gamma correction match the non-linear characteristics of luminance.
    Arg:
       img: input image data.
       bayer: bayer pattern of raw data.

    '''

    def __init__(self, img, args,lut):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.bayer = args.bayer
        self.bw = args.bw

        self.lut = lut

    def do(self):
        gc_img = np.empty((self.h,self.w, 3), np.uint16)
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                gc_img[i, j, 0] = self.lut[self.img[i, j, 0]]
                gc_img[i, j, 1] = self.lut[self.img[i, j, 1]]
                gc_img[i, j, 2] = self.lut[self.img[i, j, 2]]
                # gc_img[i, j, :] = gc_img[i, j, :] // 4  # 10 bit -> 8 bit

        np.clip(gc_img, 0, 255, out=gc_img)
        return gc_img
