#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class LSC:
    '''
    doing Lens shading.
    Arg:
       img: input image data.
       bayer: bayer pattern of raw data.

    '''

    def __init__(self, img, args, lsc_gain):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.bayer = args.bayer
        self.bw = args.bw

        self.lsc_r = lsc_gain['r']
        self.lsc_gr = lsc_gain['gr']
        self.lsc_gb = lsc_gain['gb']
        self.lsc_b = lsc_gain['b']

    def do(self):
        lsc_img = np.zeros((self.h, self.w), np.uint16)
        for i in tqdm(range(0, self.h - 1, 2)):
            for j in range(0, self.w - 1, 2):
                lsc_img[i, j] = self.lsc_r[i//2, j//2] * self.img[i, j]
                lsc_img[i, j+1] = self.lsc_gr[i // 2, (j+1) // 2] * self.img[i, j+1]
                lsc_img[i+1, j] = self.lsc_gb[(i+1) // 2, j // 2] * self.img[i+1, j]
                lsc_img[i+1, j+1] = self.lsc_b[(i+1) // 2, (j+1) // 2] * self.img[i+1, j+1]
            # lsc_img = lsc_img.astype(np.uint16)
        #lsc_img = (lsc_img - np.min(lsc_img)) * self.bw / (np.max(lsc_img) - np.min(lsc_img))
        lsc_img = lsc_img.astype(np.uint16)
        np.clip(lsc_img, 0, self.bw, out=lsc_img)
        return lsc_img
