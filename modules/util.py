#!/usr/bin/env python
# -*- coding:utf-8 -*-
import numpy as np
import cv2

def read_uint10(byte_buf):
    '''
    args:
        byte_buf: input raw mipi file path.
    return:
          the 10 bit raw data.
    '''
    data = np.fromfile(byte_buf,dtype='uint8')
    # 5 bytes contain 4 10-bit pixels (5x8 == 4x10)
    b1, b2, b3, b4, b5 = np.reshape(data, (data.shape[0]//5, 5)).astype(np.uint16).T #the number of 5-bytes
    o1 = (b1 << 2) + ((b5) & 0x3)
    o2 = (b2 << 2) + ((b5 >> 2) & 0x3)
    o3 = (b3 << 2) + ((b5 >> 4) & 0x3)
    o4 = (b4 << 2) + ((b5 >> 6) & 0x3)
    unpacked = np.reshape(np.concatenate((o1[:, None], o2[:, None], o3[:, None], o4[:, None]), axis=1), (3000, 4000))
    return unpacked


def save_rgb_img(save_path,rgb_img):
    # norm_img = cv2.normalize(rgb_img, 0, 255, cv2.NORM_MINMAX)
    norm_img = rgb_img.astype(np.uint8)
    bgr_img = cv2.cvtColor(norm_img, cv2.COLOR_RGB2BGR)
    cv2.imwrite(save_path, bgr_img)

def save_yuv_img(save_path, y, uv):
    yuvimg_out = np.empty((y.shape[0], y.shape[1], 3), dtype=np.uint8)
    bcc_yuvimg = y
    tmp = np.copy(uv[:, :, 0])
    uv[:, :, 0] = uv[:, :, 1]
    uv[:, :, 1] = tmp
    hsc_yuvimg = uv
    yuvimg_out[:, :, 0] = bcc_yuvimg
    yuvimg_out[:, :, 1:3] = hsc_yuvimg
    bgr_img = cv2.cvtColor(yuvimg_out, cv2.COLOR_YCrCb2BGR)
    cv2.imwrite(save_path, bgr_img)

def to_uint8(img):
    img *= (img > 0)
    img = img * (img <= 255) + 255 * (img > 255)
    img = img.astype(np.uint8)

def get_block_interplate(img_w, img_h, lsc):
    '''
    args:
        img_w: input raw image's weight
        img_h: input raw image's height
        lsc: input lsc gain block matrix
    return:
        save npz of interplated lsc gain matrix
    '''
    # get R, Gr, Gb, B's bilinear interplate value
    lsc_r, lsc_gr, lsc_gb, lsc_b = lsc['r'], lsc['gr'], lsc['gb'], lsc['b']
    lsc_h, lsc_w = lsc_r.shape[0] - 1, lsc_r.shape[1] - 1
    interval_h, interval_w = img_h // 2 // lsc_h, img_w // 2 // lsc_w # get numbers of block
    print(interval_h,interval_w)
    lsc_r = lsc_r / np.min(lsc_r) # normalize lsc matrix, which the center to 1 and corner bigger than 1
    lsc_gr = lsc_gr / np.min(lsc_gr)
    lsc_gb = lsc_gb / np.min(lsc_gb)
    lsc_b = lsc_b / np.min(lsc_b)

    inter_r = np.zeros((img_h//2, img_w//2)) # the interpolated result
    inter_gr = np.zeros((img_h//2, img_w//2))
    inter_gb = np.zeros((img_h//2, img_w//2))
    inter_b = np.zeros((img_h//2, img_w//2))

    # each block's 4 corner point
    bx0, bx1, by0, by1 = 0, interval_w-1, 0, interval_h-1
    for i in range(img_h//2):
        for j in range(img_w//2):
            block_y, block_x = i // interval_h, j // interval_w # locate which block
            cur_y, cur_x = i - block_y * interval_h, j - block_x * interval_w # relative coordinate in block

            p1, p2, p3, p4 = \
            	lsc_r[block_y, block_x], lsc_r[block_y+1, block_x], lsc_r[block_y, block_x+1], lsc_r[block_y+1, block_x+1]
            # bilinear weight
            w1, w2, w3, w4 = (bx1 - cur_x) * (by1 - cur_y), (bx1 - cur_x) * (cur_y - by0),\
            				 (cur_x - bx0) * (by1 - cur_y), (cur_x - bx0) * (cur_y - by0)
            #print(j+block_x*interval_w)
            inter_r[i, j] = ( w1 * p1 + w2 * p2 + w3 * p3 + w4 * p4 ) / ( interval_h * interval_w )
            # print(block_x,interval_w, j+block_x*interval_w)
            p1, p2, p3, p4 = \
            	lsc_gr[block_y, block_x], lsc_gr[block_y + 1, block_x], lsc_gr[block_y, block_x + 1], lsc_gr[block_y + 1, block_x + 1]
            inter_gr[i, j] = (w1 * p1 + w2 * p2 + w3 * p3 + w4 * p4) / (interval_h * interval_w)

            p1, p2, p3, p4 = \
            	lsc_gb[block_y, block_x], lsc_gb[block_y + 1, block_x], lsc_gb[block_y, block_x + 1], lsc_gb[
            		block_y + 1, block_x + 1]
            inter_gb[i, j] = (w1 * p1 + w2 * p2 + w3 * p3 + w4 * p4) / (interval_h * interval_w)

            p1, p2, p3, p4 = \
            	lsc_b[block_y, block_x], lsc_b[block_y + 1, block_x], lsc_b[block_y, block_x + 1], lsc_b[
            		block_y + 1, block_x + 1]
            inter_b[i, j] = (w1 * p1 + w2 * p2 + w3 * p3 + w4 * p4) / (interval_h * interval_w)

    np.savez('./modules/lsc_gain_interp', r=inter_r, gr=inter_gr, gb=inter_gb, b=inter_b)

