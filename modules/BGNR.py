#!/usr/bin/env python
# -*- coding:utf-8 -*-
import numpy as np
from tqdm import tqdm
import math

class BGNR:
    '''
    Use bilateral gaussian Denoising apply in Y-channel.
    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]

        self.diameter = args.diameter
        self.color_sigma = args.color_sigma
        self.space_sigma = args.space_sigma

    def do(self):
        radius = int(self.diameter / 2)
        nr_img = np.zeros((self.h, self.w))
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                current_pixel_filtered = 0
                weight_sum = 0  # for normalize
                for semi_row in range(-radius, radius + 1):
                    for semi_col in range(-radius, radius + 1):
                        # calculate the convolution by traversing each close pixel within radius
                        if i + semi_row >= 0 and i + semi_row < self.h:
                            row_offset = i + semi_row
                        else:
                            row_offset = 0
                        if semi_col + j >= 0 and semi_col + j < self.w:
                            col_offset = j + semi_col
                        else:
                            col_offset = 0
                        color_weight = self.gaussian(int(self.img[row_offset][col_offset]) - int(self.img[i][j]), self.color_sigma)
                        space_weight = self.gaussian(self.distance(row_offset, col_offset, i, j), self.space_sigma)
                        weight = space_weight * color_weight
                        current_pixel_filtered += self.img[row_offset][col_offset] * weight
                        weight_sum += weight

                current_pixel_filtered = current_pixel_filtered // weight_sum
                nr_img[i, j] = current_pixel_filtered
        np.clip(nr_img, 0, 255, out=nr_img)
        return nr_img

    def distance(self, x, y, i, j):
        return np.sqrt((x - i) ** 2 + (y - j) ** 2)

    def gaussian(self, x, sigma):
        return (1.0 / (2 * math.pi * (sigma ** 2))) * math.exp(- (x ** 2) / (2 * sigma ** 2))



