#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class BCC:
    '''
    Brightness and Contrast Control.
    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.brightness = args.bright
        self.contrast = args.contrast / pow(2, 5)

    def execute(self):
        bcc_img = np.empty((self.h, self.w), np.int16)
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                bcc_img[i, j] = self.img[i, j] + self.brightness
                bcc_img[i, j] = bcc_img[i, j] + (bcc_img[i, j] - 127) * self.contrast
        np.clip(bcc_img, 0, 255, out=bcc_img)
        return bcc_img
