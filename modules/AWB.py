#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class AWB:
    '''
    doing auto white balance.
    Arg:
       img: input image data.

    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.bayer = args.bayer
        self.bw = args.bw

        self.r_gain = args.r_gain
        self.gr_gain = args.gr_gain
        self.gb_gain = args.gb_gain
        self.b_gain = args.b_gain

    def do(self):
        '''
        R' = R * (R_avg / G_avg) * gain = R * r_gain
        Gr' = Gr * gain = Gr * gr_gain
        Gb' = Gb * gain = Gb * gb_gain
        B' = B * (B_avg / G_avg) * gain = B * b_gain
        and want to get R' = G' = B'

        Return:
              the processed image
        '''

        awb_img = np.zeros((self.h, self.w))
        for i in tqdm(range(0, self.h-1,2)):
            for j in range(0, self.w-1,2):
                if self.bayer == 'rggb':
                    r = self.img[i, j] * self.r_gain
                    gr = self.img[i, j+1] * self.gr_gain
                    gb = self.img[i+1, j] * self.gb_gain
                    b = self.img[i+1, j+1] * self.b_gain

                    awb_img[i, j] = r
                    awb_img[i, j+1] = gr
                    awb_img[i+1, j] = gb
                    awb_img[i+1, j+1] = b

        np.clip(awb_img, 0, self.bw, out=awb_img)
        return awb_img
