#!/usr/bin/env python
# -*- coding:utf-8 -*-
import numpy as np
from tqdm import tqdm

class EE:
    '''
    Use bilateral gaussian Denoising apply in Y-channel.
    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]

        self.ex = args.extent

    def do(self):
        ee_img = self.sobel_filter()
        return ee_img

    def sobel_filter(self):

        ee_img = np.zeros((self.h, self.w), np.uint16)
        pad_img = np.pad(self.img, (1, 1), 'reflect').astype(np.int32)
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                sx = (pad_img[i + 1][j - 1] + 2 * pad_img[i + 1][j] + pad_img[i + 1][j + 1]) - \
                     (pad_img[i - 1][j - 1] + 2 * pad_img[i - 1][j] + pad_img[i - 1][j + 1])
                sy = (pad_img[i - 1][j + 1] + 2 * pad_img[i][j + 1] + pad_img[i + 1][j + 1]) - \
                     (pad_img[i - 1][j - 1] + 2 * pad_img[i][j - 1] + pad_img[i + 1][j - 1])
                ee_img[i][j] = np.sqrt(np.square(sx) + np.square(sy))
        np.clip(ee_img, 0, 255, out=ee_img)
        self.img = (1 - self.ex) * self.img + self.ex * ee_img
        return self.img

