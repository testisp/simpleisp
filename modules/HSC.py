#!/usr/bin/env python
# -*- coding:utf-8 -*-
import numpy as np
from tqdm import tqdm

class HSC:
    '''
    Hue Saturation Control
    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]

        self.hue = args.hue
        self.saturation = args.saturation

    def execute(self):
        lut_sin, lut_cos = self.lut()
        hsc_img = np.zeros((self.h, self.w, 2), np.int16)
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                hsc_img[i, j, 0] = (self.img[i, j, 0] - 128) * lut_cos[self.hue] + (self.img[i, j, 1] - 128) * lut_sin[self.hue] + 128
                hsc_img[i, j, 1] = (self.img[i, j, 1] - 128) * lut_cos[self.hue] - (self.img[i, j, 0] - 128) * lut_sin[self.hue] + 128
                hsc_img[i, j, 0] = self.saturation * (hsc_img[i, j, 0] - 128) / 256 + 128
                hsc_img[i, j, 1] = self.saturation * (hsc_img[i, j, 1] - 128) / 256 + 128
        np.clip(hsc_img, 0, 255, out=hsc_img)
        return hsc_img

    def lut(self):
        ind = np.array([i for i in range(360)])
        sin = np.sin(ind * np.pi / 180) * 256
        cos = np.cos(ind * np.pi / 180) * 256
        lut_sin = dict(zip(ind, [round(sin[i]) for i in ind]))
        lut_cos = dict(zip(ind, [round(cos[i]) for i in ind]))
        return lut_sin, lut_cos