#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class CSC:
    '''

    Arg:
       img: input image data.
       bayer: bayer pattern of raw data.

    '''

    def __init__(self, img):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]

        # ITU.BT-601 
        self.transform_matrix = np.array([[0.257, 0.504, 0.098],
                                          [-0.148, -0.291, 0.439],
                                          [0.439, -0.368, -0.071]])
        self.shift_matrix = np.array([16, 128, 128])


    def do(self):
        ycbcr_img = np.empty((self.h, self.w, 3), np.uint32)
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                ycbcr_img[i, j, :] = np.dot(self.transform_matrix, self.img[i, j, :]) + self.shift_matrix
        ycbcr_img = ycbcr_img.astype(np.uint8)
        return ycbcr_img
