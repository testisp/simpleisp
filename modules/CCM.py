#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class CCM:
    '''
    doing Color Correction Matrix.
    Arg:
       img: input raw data.

    '''
    def __init__(self, img, color_matrix):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.ccm = color_matrix

    def do(self):
        ccm_img = np.zeros((self.h, self.w, 3), np.uint32)
        for i in tqdm(range(self.h)):
            for j in range(self.w):
                ccm_img[i, j, :] = np.dot(self.ccm, self.img[i, j, :])
        ccm_img = ccm_img.astype(np.uint8)
        return ccm_img