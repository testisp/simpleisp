﻿#include "LSC.h"



uint32_t GetinterpValue(const InterpPara& interppara, char* channel, const BlcokCorner& corner) {

    // get the 4 corner coordinates at red channel
    uint32_t p1 = 0, p2 = 0, p3 = 0, p4 = 0;
    char ch[4][3] = {"r","gr","gb","b"};
    if (strcmp(channel, ch[0])) {
        p1 = g_lsc.rGain[interppara.blockY][interppara.blockX];
        p2 = g_lsc.rGain[interppara.blockY + 1][interppara.blockX];
        p3 = g_lsc.rGain[interppara.blockY][interppara.blockX + 1];
        p4 = g_lsc.rGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    else if (strcmp(channel, ch[1])) {
        p1 = g_lsc.grGain[interppara.blockY][interppara.blockX];
        p2 = g_lsc.grGain[interppara.blockY + 1][interppara.blockX];
        p3 = g_lsc.grGain[interppara.blockY][interppara.blockX + 1];
        p4 = g_lsc.grGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    else if (strcmp(channel, ch[2])) {
        p1 = g_lsc.gbGain[interppara.blockY][interppara.blockX];
        p2 = g_lsc.gbGain[interppara.blockY + 1][interppara.blockX];
        p3 = g_lsc.gbGain[interppara.blockY][interppara.blockX + 1];
        p4 = g_lsc.gbGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    else if (strcmp(channel, ch[3])) {
        p1 = g_lsc.bGain[interppara.blockY][interppara.blockX];
        p2 = g_lsc.bGain[interppara.blockY + 1][interppara.blockX];
        p3 = g_lsc.bGain[interppara.blockY][interppara.blockX + 1];
        p4 = g_lsc.bGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    uint32_t avg = interppara.w1 * p1 + interppara.w2 * p2 + interppara.w3 * p3 + interppara.w4 * p4;
    return avg;
}


void ExcuteProcess(uint16_t* blcdata, uint32_t weidth, uint32_t height) {
    uint16_t* rawdata = blcdata;
    uint32_t imwidth = weidth;
    uint32_t imheight = height;
    uint32_t halfW = imwidth >> 1;
    uint32_t halfH = imheight >> 1;

    //numbers of block
    uint32_t intervalH = SimpleDivid(halfH, g_lsc.numH - 1);
    uint32_t intervalW = SimpleDivid(halfW, g_lsc.numW - 1);
    uint32_t partition = intervalH * intervalW; // bilinear interplate partition coefficient
    Interpchannel interpchannel;
    interpchannel.rInterp = new uint32_t * [halfH];
    interpchannel.grInterp = new uint32_t * [halfH];
    interpchannel.gbInterp = new uint32_t * [halfH];
    interpchannel.bInterp = new uint32_t * [halfH];
    // block's 4 corner point coordinates
    BlcokCorner corner;
    corner.bx0 = 0;
    corner.bx1 = intervalW - 1;
    corner.by0 = 0;
    corner.by1 = intervalH - 1;
    InterpPara interppara;
    // using fix-point as Q10 Calibrations  
    for (uint32_t i = 0; i < halfH; i++) {
        interpchannel.rInterp[i] = new uint32_t[halfW];
        interpchannel.grInterp[i] = new uint32_t[halfW];
        interpchannel.gbInterp[i] = new uint32_t[halfW];
        interpchannel.bInterp[i] = new uint32_t[halfW];
        for (uint32_t j = 0; j < halfW; j++) {
            // locate which block
            interppara.blockY = SimpleDivid(i, intervalH);
            interppara.blockX = SimpleDivid(j, intervalW);
            // relative coordinate in block
            interppara.curY = i - interppara.blockY * intervalH;
            interppara.curX = j - interppara.blockX * intervalW;
            // calculate the bilinear weight
            interppara.w1 = (corner.bx1 - interppara.curX) * (corner.by1 - interppara.curY);
            interppara.w2 = (corner.bx1 - interppara.curX) * (interppara.curY - corner.by0);
            interppara.w3 = (interppara.curX - corner.bx0) * (corner.by1 - interppara.curY);
            interppara.w4 = (interppara.curX - corner.bx0) * (interppara.curY - corner.by0);

            uint32_t avg;
            uint32_t norm;
            // interplate r channel lsc coefficient
            avg = GetinterpValue(interppara, (char*)"r", corner);
            norm = Divid(avg, partition);
            interpchannel.rInterp[i][j] = norm;
            //interplate gr channel lsc coefficient
            avg = GetinterpValue(interppara, (char*)"gr", corner);
            norm = Divid(avg, partition);
            interpchannel.grInterp[i][j] = norm;
            // interplate gb channel lsc coefficient
            avg = GetinterpValue(interppara, (char*)"gb", corner);
            norm = Divid(avg, partition);
            interpchannel.gbInterp[i][j] = norm;
            // interplate b channel lsc coefficient
            avg = GetinterpValue(interppara, (char*)"b", corner);
            norm = Divid(avg, partition);
            interpchannel.bInterp[i][j] = norm;

            rawdata[2 * imwidth * i + 2 * j] =
                (uint32_t)((float)rawdata[2 * imwidth * i + 2 * j] * ((float)interpchannel.rInterp[i][j] * reciprocallut[9])); // r
            rawdata[2 * imwidth * i + 2 * j + 1] =
                (uint32_t)(rawdata[2 * imwidth * i + 2 * j + 1] * ((float)interpchannel.grInterp[i][j] * reciprocallut[9])); // gr
            rawdata[(2 * i + 1) * imwidth + 2 * j] =
                (uint32_t)((float)rawdata[(2 * i + 1) * imwidth + 2 * j] * ((float)interpchannel.gbInterp[i][j] * reciprocallut[9])); // gb
            rawdata[(2 * i + 1) * imwidth + 2 * j + 1] =
                (uint32_t)((float)rawdata[(2 * i + 1) * imwidth + 2 * j + 1] * ((float)interpchannel.bInterp[i][j] * reciprocallut[9])); // b  

        }
    }
    delete interpchannel.rInterp;
    delete interpchannel.grInterp;
    delete interpchannel.gbInterp;
    delete interpchannel.bInterp;

}


uint32_t SimpleDivid(const uint32_t& dividend, const uint32_t& divisor) {
    uint32_t ret = 0;
    if (dividend < divisor) return ret;
    uint32_t x = dividend;
    uint32_t y = divisor;
    while (x >= y)
    {
        x -= y;
        ++ret;
    }
    return ret;
}

uint32_t Divid(const uint32_t& dividend, const uint32_t& divisor) {

    long dvd = (long)dividend, dvs = (long)divisor, ans = 0;
    while (dvd >= dvs) {
        long temp = dvs, m = 1;
        while (temp << 1 <= dvd) {
            temp <<= 1;
            m <<= 1;
        }
        dvd -= temp;
        ans += m;
    }
    return ans;
}
