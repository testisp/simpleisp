#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class Demosaic:
    '''
    doing Color Filter Array Interpolation (CFA) or called demosaic.
    Arg:
       img: input image data.
       bayer: bayer pattern of raw data.

    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.bayer = args.bayer
        self.bw = args.bw

        self.interplate = args.interplate




    def do(self):
        '''

        Returns:
            the compensationed data.
        '''

        dem_img = np.zeros((self.h, self.w, 3), np.int16)
        pad_img = np.pad(self.img, (2, 2), 'reflect').astype(np.int16)
        if self.bayer == 'rggb':
            for i in tqdm(range(0, pad_img.shape[0] - 4 - 1, 2)):
                for j in range(0, pad_img.shape[1] - 4 - 1, 2):
                    if self.interplate == 'bilinear':
                        dem_img[i, j, :] = self.bilinear_interpolate(pad_img, i + 2, j + 2, 'red')
                        dem_img[i, j + 1, :] = self.bilinear_interpolate(pad_img, i + 2, j + 3, 'gr')
                        dem_img[i + 1, j, :] = self.bilinear_interpolate(pad_img, i + 3, j + 2, 'gb')
                        dem_img[i + 1, j + 1, :] = self.bilinear_interpolate(pad_img, i + 3, j + 3, 'blue')
                    elif self.interplate == 'malvar':
                        dem_img[i, j, :] = self.malvar_interpolate(pad_img, i + 2, j + 2, 'red')
                        dem_img[i, j + 1, :] = self.malvar_interpolate(pad_img, i + 2, j + 3, 'gr')
                        dem_img[i + 1, j, :] = self.malvar_interpolate(pad_img, i + 3, j + 2, 'gb')
                        dem_img[i + 1, j + 1, :] = self.malvar_interpolate(pad_img, i + 3, j + 3, 'blue')
        np.clip(dem_img, 0, self.bw, out=dem_img)
        dem_img = dem_img // 4 # 10 bit -> 8 bit

        return dem_img

    def bilinear_interpolate(self, img, i, j, color):
        r, g, b = 0, 0, 0
        if color == 'red':
            g = (img[i-1, j] + img[i, j-1] + img[i, j+1] + img[i+1, j]) / 4.0
            b = (img[i-1, j-1] + img[i-1, j+1] + img[i+1, j-1] + img[i+1, j+1]) / 4.0
            r = img[i, j]
        elif color == 'blue':
            g = (img[i-1, j] + img[i, j-1] + img[i, j+1] + img[i+1, j]) / 4.0
            r = (img[i-1, j-1] + img[i-1, j+1] + img[i+1, j-1] + img[i+1, j+1]) / 4.0
            b = img[i, j]
        elif color == 'gr':
            r = (img[i, j-1] + img[i, j+1]) / 2.0
            b = (img[i-1, j] + img[i+1, j]) / 2.0
            g = img[i, j]
        elif color == 'gb':
            r = (img[i - 1, j] + img[i + 1, j]) / 2.0
            b = (img[i, j - 1] + img[i, j + 1]) / 2.0
            g = img[i, j]

        return [r, g, b]

    def malvar_interpolate(self,img,i,j,color):
        '''
        paper: https://stanford.edu/class/ee367/reading/Demosaicing_ICASSP04.pdf
        or https://blog.csdn.net/qq_42722197/article/details/112598391
        Using its neighboring pixels and nearby pixels of the same channel, called gradient corrected interpolation

        args:
            img: input image
            i,j: the index of the current center pixel
            color: the color channel in color filter

        Returns:
            demosaic image used malvar filter.
        '''

        if color == 'red':
            g = 4 * img[i,j] + 2 * (img[i+1,j] + img[i-1,j] + img[i,j+1] + img[i,j-1]) \
                - img[i+2,j] - img[i-2,j] - img[i,j+2] - img[i,j-2]
            b = 6 * img[i,j] + 2 * (img[i-1,j-1] + img[i-1,j+1] + img[i+1,j-1] + img[i+1,j+1]) \
                - 3 * (img[i+2,j] + img[i-2,j] + img[i,j+2] + img[i,j-2]) / 2
            r = img[i,j]
            g /= 8
            b /= 8
        elif color == 'blue':
            g = 4 * img[i, j] + 2 * (img[i + 1, j] + img[i - 1, j] + img[i, j + 1] + img[i, j - 1]) \
                - img[i + 2, j] - img[i - 2, j] - img[i, j + 2] - img[i, j - 2]
            r = 6 * img[i, j] + 2 * (img[i - 1, j - 1] + img[i - 1, j + 1] + img[i + 1, j - 1] + img[i + 1, j + 1]) \
                - 3 * (img[i + 2, j] + img[i - 2, j] + img[i, j + 2] + img[i, j - 2]) / 2
            b = img[i, j]
            g /= 8
            r /= 8
        elif color == 'gr':
            r = 5 * img[i,j] + 4 * (img[i,j-1] + img[i,j+1]) - img[i-1,j-1] - img[i-1,j+1] - img[i+1,j-1] - img[i+1,j+1] \
                + (img[i+2,j] + img[i-2,j]) / 2 - img[i,j+2] - img[i,j-2]
            b = 5 * img[i,j] + 4 * (img[i-1,j] + img[i+1,j]) - img[i-1,j-1] - img[i-1,j+1] - img[i+1,j-1] - img[i+1,j+1] \
                - img[i+2,j] - img[i-2,j] + (img[i,j+2] + img[i,j-2]) / 2
            g = img[i,j]
            r /= 8
            b /= 8
        else: # gb channel
            r = 5 * img[i,j] + 4 * (img[i-1,j] + img[i+1,j]) - img[i-1,j-1] - img[i-1,j+1] - img[i+1,j-1] - img[i+1,j+1] \
                - img[i+2,j] - img[i-2,j] + (img[i,j+2] + img[i,j-2]) / 2
            b = 5 * img[i,j] + 4 * (img[i,j-1] + img[i,j+1]) - img[i-1,j-1] - img[i-1,j+1] - img[i+1,j-1] - img[i+1,j+1] \
                + (img[i+2,j] + img[i-2,j]) / 2 - img[i,j+2] - img[i,j-2]
            g = img[i, j]
            r /= 8
            b /= 8

        return [r, g, b]