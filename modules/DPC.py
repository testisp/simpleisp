#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
from tqdm import tqdm

class DPC:
    '''
    doing dead pixel correction.
    Arg:
       img: input raw data.
       thd: the threshold for difference of current pixel with neiborhood.

    '''
    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.thd = args.dpc_thd
        self.bw = args.bw

    def do(self):
        '''
        1 - 2 - 3
        - - - - -
        4 - 0 - 5
        - - - - -
        6 - 7 - 8
        diff = (pi - p0) and if && (diff > threshold ) , do mean filter for p0.

        Returns:
            the correctioned raw data
        '''
        pad_img = np.pad(self.img, (2,2), 'reflect').astype(np.int16)
        print(pad_img.shape)
        h = pad_img.shape[0]
        w = pad_img.shape[1]
        wind = np.zeros(9)
        dpc_img = np.zeros((self.h, self.w))
        for i in tqdm(range(h-4)):
            for j in range(w-4):
                wind[0], wind[1], wind[2] = pad_img[i+2, j+2], pad_img[i, j], pad_img[i, j+2]
                wind[3], wind[4], wind[5] = pad_img[i, j+4], pad_img[i+2, j], pad_img[i+2, j+4]
                wind[6], wind[7], wind[8] = pad_img[i+4, j], pad_img[i+4, j+2], pad_img[i+4, j+4]

                for n in range(1,9):
                    # if the number of pixels bigger than the threshold is more than one, do mean filter
                    if abs(wind[n] - wind[0]) > self.thd:
                        dpc_img[i][j] = ( wind[1] + wind[2] + wind[3] + wind[4]
                                                 + wind[5] + wind[6] + wind[7] + wind[8]) / 8
                        break
                    else:
                        dpc_img[i][j] = wind[0]
        #do cliping ?
        np.clip(dpc_img, 0, self.bw, out=dpc_img)
        return dpc_img









