#!/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np

class BLC:
    '''
    doing black level compensation.
    Arg:
       img: input image data.
       r_offset: black level offset of R channel.
       gr_offset: black level offset of Gr channel
       gb_offset: black level offset of Gb channel
       b_offset: black level offset of B channel
       alpha: fusion parameter for R channel
       beta: fusion parameter for B channel.s
       bayer: bayer pattern of raw data.
    '''

    def __init__(self, img, args):
        self.img = img
        self.h = img.shape[0]
        self.w = img.shape[1]
        self.bw = args.bw
        self.bayer = args.bayer

        self.r_offset = args.r_offset
        self.gr_offset = args.gr_offset
        self.gb_offset = args.gb_offset
        self.b_offset = args.b_offset
        self.alpha = args.alpha
        self.beta = args.beta


    def do(self):
        '''
        R' = R + R_offset
        Gr' = Gr + Gr_offset + alpha * R
        Gb' = Gb + Gb_offset + beta * B
        B' = B + B_offset

        or
        all the channel substract a fixed value
        Returns:
            the compensationed data.
        '''

        # blc_img = np.zeros((self.h,self.w))
        # for i in range(0, self.h - 1, 2):
        #     for j in range(0, self.w - 1, 2):
        #         if self.bayer == 'rggb':
        #             r = self.img[i, j] + self.r_offset
        #             gr = self.img[i, j + 1] + self.gr_offset + self.alpha * self.img[i, j]
        #             gb = self.img[i+1, j] + self.gb_offset + self.beta * self.img[i+1, j+1]
        #             b = self.img[i+1, j+1] + self.b_offset
        #
        #             blc_img[i, j] = r
        #             blc_img[i, j + 1] = gr
        #             blc_img[i + 1, j] = gb
        #             blc_img[i + 1, j + 1] = b

        # if blc is a fix value
        blc_img = self.img + self.r_offset
        np.clip(blc_img, 0, self.bw, out=blc_img)
        return blc_img

